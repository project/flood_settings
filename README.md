CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Flood Settings module provides a UI to manage the flood settings provided by the core user module.

 * For a full description of the module visit:
   https://www.drupal.org/project/flood_settings

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/flood_settings


REQUIREMENTS
------------

PHP 7.1+


INSTALLATION
------------

 * Install the Flood Settings module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


CONFIGURATION
-------------

Navigate to Administration > Configuration > System > Flood Settings (`/admin/config/system/flood`).


MAINTAINERS
-----------

 * Christophe Klein (christophe.klein) - https://www.drupal.org/u/christopheklein

Supporting organization:

 * Actency - https://www.drupal.org/actency
